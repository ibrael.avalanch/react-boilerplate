import {combineReducers, createStore, applyMiddleware, compose} from 'redux';
import thunk from 'redux-thunk';
import createLogger from 'redux-logger';
import promiseMiddleware from 'redux-promise';

import reducers from './reducers'


export default function configureStore (initialState = {})  {

    const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

    const enhancer = composeEnhancers (
        applyMiddleware(
            thunk,
            createLogger(),
            promiseMiddleware
        )
    )


    const store = createStore(
        reducers,
        initialState,
        enhancer
    )

    if(module.hot) {

        module.hot.accept('./reducers', () => {
            const nextReducer = require('./reducers');

            store.replaceReducer(nextReducer);
        });

    }

    return store
}

